# NickModules Todo

	## Overall
	[TODO] Add some namespacing.
	[TODO] Optimize the folder structure, it will take som time.

	## Routing: Error fixing and optimization
		[TODO] Fix the error that occurs when NickModules is placed in a sub directory.
		[TODO] Maybe add the possibility to name a route via Route::get('/' [...])->name('someRoute')	
			   Just took a look at this.. This will take a full rewrite of the Route controller! FML.
		[TODO] Take a look at managing subdomains via the routing system.
			   No!
		[TODO] Add a resource method, so i don't have to repeat myself.
			   Hell yes.

	## Request:
		[TODO] Make it possible to send fields and errors through the redirect method.
			   ->withInput(Input::all())
			   ->withErrors(Input::all())
