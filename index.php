<?php

    // No error reporting.
    error_reporting(0);

    // Load the kernel where we've required all classes.
    require_once("App/kernel.php");

    // Load the current defined routes.
    require_once("App/routes.php");

    // Make sure that we get the current folder,
    // so we have a perfectly working site.
    $routing = new Route(basename(__DIR__));

    // Load the routing part!
    // It almost works better than laravels routing. JK.
    return $routing->brainTwist();

?>
