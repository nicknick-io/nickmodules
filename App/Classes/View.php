<?php

    class View {

        public static $data = [
            'with' => [],
            'file' => null
        ];

        public static function make($file) {

            # Get the $file variable.
            # And then explode it by '.'
            $folderAndFile = explode('.', $file);

            # Now.. Implode the exact same data and move on.
            $string = implode('/', $folderAndFile);

            // Define exactly where the file is.
            $file = Self::location() . DIRECTORY_SEPARATOR . $string . '.blade.php';

            // Set the filename.
            Self::$data['file'] = $file;

            return new static;

        }

        public static function file() {

            //
            $file = Self::$data['file'];
            $contents = file_get_contents($file);

            return $contents;
        }

        function __destruct() {

            // Get the file.
            $file = Self::file();

            // Send the file contents through bladeHandler()
            return Self::bladeHandler($file);

        }

        public function with($with) {

            if (is_array($with)) {

                // Put the data into the array.
                foreach($with as $key => $d) {

                    Self::$data['with'][$key] = $d;

                }

            }

            // Return the function so we can
            // methodchain the shit out of it.
            return $this;

        }


        public static function rules($html, $type) {

            // Make a variable for the manipulated html.
            $manipulated = $html;

            switch($type) {

                case 'extends':

                    $pattern = '/\@extends\("(.*?)"\)/';
                    preg_match_all($pattern, $html, $matches);
                    foreach($matches[0] as $key => $match) {

                        # I just fixed @extends.. You can quit using the full path for a view.
                        $TheFilePath = $matches[1][$key];
                        $TheFilePath = explode('.', $TheFilePath);
                        $TheFilePath = implode('/', $TheFilePath);
                        $TheFilePath = $TheFilePath . '.blade.php';

                        $link = 'App/Views/' . $TheFilePath;
                        $file = fopen($link, "r") or die("Blade not found: " . $file);
                        $test = fread($file, filesize($link));

                        $manipulated = str_replace($match, $test, $manipulated);

                        fclose($file);
                    }

                    preg_match_all($pattern, $manipulated, $matches);
                    if (count($matches[0]) > 0) {
                        $manipulated = Self::rules($manipulated, 'extends');
                    }

                break;

                case 'outputs':

                    $pattern = ['/{{ (.*?) }}/', '/{{(.*?)}}/'];
                    $replacement = '<?php print($1); ?>';
                    $html = preg_replace($pattern, $replacement, $html);

                    $manipulated = $html;

                break;

                case 'execute':

                    $pattern = ['/{ (.*?) }/', '/{(.*?)}/'];
                    $replacement = '<?php $1; ?>';
                    $html = preg_replace($pattern, $replacement, $html);

                    $manipulated = $html;

                break;

                case 'foreach':

                    // foreach($messages as $message):
                        $pattern = '/@foreach\((.*?) as (.*?)\)/';
                        $replacement = '<?php foreach($1 as $2): ?>';
                        $html = preg_replace($pattern, $replacement, $html);

                        $manipulated = $html;

                    // foreach($messages as $key => $message):
                        $pattern = '/@foreach\((.*?) as (.*?) => (.*?)\)/';
                        $replacement = '<?php foreach($1 as $2 => $3): ?>';
                        $html = preg_replace($pattern, $replacement, $html);

                        $manipulated = $html;

                    // endforeach;
                        $pattern = '/@endforeach/';
                        $replacement = '<?php endforeach; ?>';
                        $html = preg_replace($pattern, $replacement, $html);

                        $manipulated = $html;

                break;

                case 'for':

                    // for($messages as $message):
                    $pattern = [
                        '/\@for\((.[a-zA-Z].*) (.[a-zA-Z].*) (.[a-zA-Z].*)\)/',
                        '/\@for \((.[a-zA-Z].*) (.[a-zA-Z].*) (.[a-zA-Z].*)\)/'
                    ];
                    $replacement = '<?php for($1 $2 $3): ?>';
                    $html = preg_replace($pattern, $replacement, $html);

                    $manipulated = $html;

                    // endfor
                    $pattern = '/\@endfor/';
                    $replacement = '<?php endfor; ?>';
                    $html = preg_replace($pattern, $replacement, $html);

                    $manipulated = $html;

                break;

                case 'statement':

                    // if ($1):
                        $pattern = ['/\@if\((.[a-zA-Z].*)\)/', '/\@if \((.[a-zA-Z].*)\)/'];
                        $replacement = '<?php if ($1) : ?>';
                        $html = preg_replace($pattern, $replacement, $html);

                        $manipulated = $html;

                    // elseif ($1)
                        $pattern = ['/@elseif \((.+[)"])\)/', '/@elseif\((.+[)"])\)/'];
                        $replacement = '<?php else if($1) : ?>';
                        $html = preg_replace($pattern, $replacement, $html);

                        $manipulated = $html;
 
                    // else
                        $pattern = '/@else/';
                        $replacement = '<?php else : ?>';
                        $html = preg_replace($pattern, $replacement, $html);

                        $manipulated = $html;

                    // endif
                        $pattern = '/@endif/';
                        $replacement = '<?php endif; ?>';
                        $html = preg_replace($pattern, $replacement, $html);

                        $manipulated = $html;

                break;

            }

            return $manipulated;

        }

        public static function bladeHandler($html) {

            // Extract the array keys as variables.
                extract(Self::$data['with']);

            // Run the rules!
                $html = Self::rules($html, 'extends');   // Extends the files before anything else.
                $html = Self::rules($html, 'outputs');   // Print outputs from the blade.
                $html = Self::rules($html, 'execute');   // Run a function from the blade.
                $html = Self::rules($html, 'statement'); // Used for if, elseif, else & endif.
                $html = Self::rules($html, 'foreach');   // Used to handle foreach.
                $html = Self::rules($html, 'for');       // Used to handle for loops.



            // Return the html!
            echo eval('?>' . $html);

            // Check if the fields is sent with the redirect.
            if (isset($_SESSION['blade']['input'])) {
                // Unset the fields from earlier.
                unset($_SESSION['blade']['input']);
            }


            if (isset($_SESSION['blade']['errors'])) {
                // Unset the errors after we have used them.
                unset($_SESSION['blade']['errors']);
            }

            return false;

        }

        public static function location() {

            // Get the full path and remove '\App\Classes\View.php'
            $remove = DIRECTORY_SEPARATOR.'App'.DIRECTORY_SEPARATOR.'Classes'.DIRECTORY_SEPARATOR.'View.php';
            $path = str_replace($remove, '', __FILE__);

            $path = $path . DIRECTORY_SEPARATOR . 'App' . DIRECTORY_SEPARATOR . 'Views' . DIRECTORY_SEPARATOR;
            // Return the path so we can use it
            // for something useful.
            return $path;

        }

    }
