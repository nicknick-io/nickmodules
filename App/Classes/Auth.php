<?php

	class Auth {

		/**
		*	@param 	| $fields - ARRAY
		*	@param 	| $table  - STRING or NULL
		* 	@return | VOID
		*/
		public static function attempt($fields, $table = null) {

			$table = ($table == null)?"users":$table;

			$password = array();
			foreach($fields as $key => $field) {
				$check = strpos($key, 'pass');
				if ($check !== false) {
					$password[$key] = $field;
					unset($fields[$key]);
				}

			}

			$user = Database::table($table)
				->where($fields)
				->first();

			foreach($password as $key => $p) {
				if (Hash::checkTwo($p, $user->$key) !== false) {
					$_SESSION['user']['id'] = $user->id;
					return true;
				} else {
					return false;
				}
			}

			return false;
		}

		/**
		*	@return VOID
		*/
		public static function check() {
			if (isset($_SESSION['user']['id'])) {
				return true;
			} else {
				return false;
			}

		}

		/**
		*	Used to fetch the information from 
		*	the user that is currently logged in.
		*
		*	@return OBJECT
		*/
		public static function user() {
			if (isset($_SESSION['user']['id'])) {
				$user = Database::table('staff')->where(['id' => $_SESSION['user']['id']])->first();
				return $user;
			}
		}

		/**
		*	Used to log out the user.
		*
		*	@return VOID
		*/
		public static function logout() {
			unset($_SESSION['user']['id']);
			return true;
		}

	}
?>
