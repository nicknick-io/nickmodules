<?php

	class Hash {

		private static $options = [
			'cost' => 10
		];

		public static function make($password) {

			$password = password_hash($password, PASSWORD_BCRYPT, Self::$options);
			return $password;

		}

		public function check($password) {
			
			$check = password_verify($password, Auth::user()->password);
			return $check;
			
		}

		public function checkTwo($password, $other) {
			
			$check = password_verify($password, $other);
			return $check;
			
		}

	}
?>