<?php

	class Mailer {

		public $values = [
			'to' => [],
			'cc' => null,
			'bcc' => null,
			'from' => [],
			'attachments' => [],
			'subject' => null,
			'replyTo' => null
		];

		/**
		*	Used to define which email address
		* 	we're trying to send an email to.
		* 	@param  string  $email
		*
		*	@return $this
		*******************************/
		public function to($email) {

			array_push($this->values['to'], $email);
			
			return $this;

		}

		/**
		*	TUsed to define which email address
		* 	we're trying to send an copy of the email to.
		* 	@param  string  $email
		*
		*	@return $this
		*******************************/
		public function cc($email) {

			$this->value('cc', $email);
			
			return $this;

		}

		public function bcc($email) {

			$this->value('bcc', $email);
			
			return $this;

		}

		/**
		*	Used to define who the email is from.
		* 	@param  string  $email
		*	@param  string  $name
		*
		*	@return $this
		*******************************/
		public function from($email, $name) {


			$this->value('from', [
				'email' => $email,
				'name' => $name
			]);

			return $this;

		}

		/**
		*	Used to attach a file.
		* 	@param  string  $url
		*	@param  string  $name
		*
		*	@return $this
		*******************************/
		public function attach($url, $name = null) {

			if (!empty($url)) {
				array_push($this->values['attachments'], [
					'name' => $name,
					'file' => $url
				]);
			}

			return $this;

		}

		/**
		*	Used to define which email address
		* 	to reply to.
		* 	@param  string  $email
		*
		*	@return $this
		*******************************/
		public function replyTo($email) {

			$this->value('replyTo', $email);

			return $this;

		}

		/**
		*	Used to define a subject
		* 	@param  string  $title
		*
		*	@return $this
		*******************************/
		public function subject($title) {
			
			$this->value('subject', $title);
			
			return $this;

		}

		public function value($input, $new = null) {

			// Check if the user wants to set the value.
			if ($new !== null){

				// Set the value
				$this->values[$input] = $new;
			
			} else {
				
				// Get the value, and return it to use.
				return $this->$values[$input];
			
			}

		}

	}

	class Mail {

		public function send($message, $data, $closure) {

			if (!is_callable($closure)) {
				throw new Exception("You need to define a closure for this to work.");
			}

			// Call the mailer / PHPMailer class.
				$mailer = new Mailer;
				$PHPMailer = new PHPMailer;

			// Set the settings.
				$env = $GLOBALS['env'];

				// $PHPMailer->SMTPDebug = 1;
				$PHPMailer->isSMTP();
				
				$PHPMailer->Host = $env['Email']['host'];
				$PHPMailer->SMTPAuth = true;
				$PHPMailer->Username = $env['Email']['username'];
				$PHPMailer->Password = $env['Email']['password'];
				$PHPMailer->SMTPSecure = "tls";
				$PHPMailer->Port = $env['Email']['port'];
				$PHPMailer->CharSet = "UTF-8";


			// Define the closure.
				$closure($mailer);

			// Use the information from the mailer class.
	
				// Check if we have a receiver.
				if ($mailer->values['to']) {
					foreach ($mailer->values['to'] as $email) {
						$PHPMailer->addAddress($email);
					}
				}

				// Check if we have a sender.
				if ($mailer->values['from']) {
					$PHPMailer->From = $mailer->values['from']['email'];
					$PHPMailer->FromName = $mailer->values['from']['name'];
				}

				// Check if we have attachments, and add them.
				if ($mailer->values['attachments']) {
					foreach($mailer->values['attachments'] as $attachment) {
						$PHPMailer->addAttachment($attachment['file'], $attachment['name']);
					}
				}

				// Check if we have a CC.
				if ($mailer->values['cc']) {
					$PHPMailer->addCC($mailer->values['cc']);
				}

				// Check if we have a BCC.
				if ($mailer->values['bcc']) {
					$PHPMailer->addBCC($mailer->values['bcc']);
				}

				// Set html to true
				$PHPMailer->isHTML(true);


				// Replace things in the message
				foreach($data as $key => $value) {
					$message = str_replace('{' . $key . '}', $value, $message);
				}

				// Set the message.
				$PHPMailer->Body = $message;

				// Set the subject.
				$PHPMailer->Subject = $mailer->values['subject'];

				if (!$PHPMailer->send()) {
					return false;
				} else {
					return true;
				}

		}

		/*public static function send($message, $data, $closure) {

			if (!is_callable($closure)) {
				throw new Exception("You need to define a closure for this to work.");
			}

			// Define the Mailer class to use within this class.
			$mailer = new Mailer();

			// Use the Mailer class through the closure.
			$closure($mailer);

			// Lets get to the mail stuff.
			$header = "";

			// Make the header.
			$from_email = $mailer->values['from']['email'];
			$from_name  = $mailer->values['from']['name'];
			$header .= "From: {$from_email} {$from_name}" . PHP_EOL;
			$header .= "Reply-To: " . $from_email . PHP_EOL;
			if ($mailer->values['cc']) {
				$cc = $mailer->values['cc'];
				$header .= "Cc: {$cc}" . PHP_EOL;
			}
			if ($mailer->values['bcc']) {
				$bcc = $mailer->values['bcc'];
				$header .= "Bcc: {$bcc}" . PHP_EOL;
			}
			$header .= "MIME-Version: 1.0" . PHP_EOL;
			$header .= "Content-type:text/html; charset=UTF-8" . PHP_EOL;

			// Subject
			$subject = $mailer->values['subject'];

			// Message
			foreach($data as $key => $value) {
				$message = str_replace('{' . $key . '}', $value, $message);
			}

			// Attachments
			foreach($mailer->values['attachments'] as $attachment) {
				// Coming when i figure out how.
			}

			// mailTo
			$mailTo = $mailer->values['to'];

			$mail = @mail($mailTo, $subject, $message, $header);
			if ($mail) {
				return true;
			} else {
				return false;
			}

		}*/

	}

?>