<?php

	class Validator {

		public static $error = [],
					  $field = [];

		public static function make($fields, $rules) {

			if (!empty($fields) && !empty($rules)) {

				Self::$field = $fields;
				Self::follow($rules);
				return new static;

			} else {

				if (!is_array($fields)) {
					echo "Validator expects parameter 1 to be array.<br>";
				}

				if (!is_array($rules)) {
					echo "Validator expects parameter 2 to be array.";
				}

			}

		}

		public static function follow($rules) {

			foreach($rules as $key => $rule) {

				$rules = explode('|', $rule);
				$field = $key;
				foreach($rules as $rule) {

					$rule = explode(':', $rule);
					$value = (isset($rule[1]))?$rule[1]:null;

					Self::rules($field, $rule[0], $value);

				}

			}

		}

		public static function rules($field, $rule, $value = null) {

			switch($rule) {

				case 'required':
					if (empty(Self::$field[$field])) {
						Self::error($field, $rule, $value);
					}
				break;

				case 'min':
					if (strlen(Self::$field[$field]) < $value) {
						Self::error($field, $rule, $value);
					}
				break;

				case 'max':
					if (strlen(Self::$field[$field]) > $value) {
						Self::error($field, $rule, $value);
					}
				break;

				case 'same':
					if (Self::$field[$field] !== Self::$field[$value]) {
						Self::error($field, $rule, $value);
					}
				break;

				case 'unique':
					$check = Database::table($value)
						->where([$field => Self::$field[$field]])
						->count();

					if ($check > 0) {
						Self::error($field, $rule, $value);
					}
				break;

				case 'digits':
					if (strlen(Self::$field[$field]) != $value) {
						Self::error($field, $rule, $value);
					}
				break;

				case 'email':
					if (!filter_var(Self::$field[$field], FILTER_VALIDATE_EMAIL)) {
						Self::error($field, $rule, $value);
					}
				break;

				case 'numeric':
					if (!is_numeric(Self::$field[$field])) {
						Self::error($field, $rule, $value);
					}
				break;

			}

		}

		public static function check() {

			if (empty(Self::$error)) {
				return true;
			} else {
				return false;
			}

		}

		public static function error($field, $rule, $value) {

			$insert = [
				'rule' => $rule,
				'value' => $value
			];

			Self::$error[$field][] = $insert;

		}

		public static function errors(array $messages) {

			# Felter.
			$fields = [];

			# Sort out the fields and rules.
			foreach($messages as $field => $message) {

				$field = explode('.', $field);

				$name = $field[0];
				$rule = $field[1];

				$field = [];
				$field['rule'] = $rule;
				$field['message'] = $message;

				$fields[$name][] = $field;

			}

			# All errors.
			$errors = [];

			# Loop through the errors.
			foreach(Self::$error as $name => $rules) {

				# Loop through the rules.
				foreach($rules as $rule) {

					# Get the error messages for the errors that occured.
					foreach($fields[$name] as $key => $message) {

						# Check if the rules match.
						if ($message['rule'] == $rule['rule']) {
							$errors[$name]['messages'][] = $fields[$name][$key];
						}

						if ($message['rule'] != $rule['rule']) {

							$errors[$name]['messages'][] = [
								'rule' => 'unknown', 
								'message' => 'Error message for <b>' . $name . '.' . $rule['rule'] . '</b> not defined.'
							];	
						
						}

					}

				}

			}

			return $errors;

		}

	}
?>
