<?php
	class Flash {

		public static $message;

		public static function make($name, $value) {

			$_SESSION['flash']['messages'][$name] = $value;
			return $_SESSION['flash']['messages'][$name];

		}

		public static function get($name) {

			Self::$message = $_SESSION['flash']['messages'][$name];
			unset($_SESSION['flash']['messages'][$name]);

			return Self::$message;

		}

		public static function has($name) {

			if (isset($_SESSION['flash']['messages'][$name])) {

				return ($_SESSION['flash']['messages'][$name])?true:false;

			}

			return false;

		}

		// This is not used yet.
		public function value($name, $value = null) {

			if ($value != null) {
				$_SESSION['flash']['messages'][$name] = $value;
			} else {
				return $_SESSION['flash']['messages'][$name];
			}

		}

	}
