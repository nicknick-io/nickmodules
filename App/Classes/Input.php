<?php

	class Input {

		public static $values = [
			'input' => [],
			'except' => [],
			'only' => [],
			'files' => []
		];

		public static function get($name, $value = null) {

			if (isset($_POST[$name])) {
				return $_POST[$name];
			} else {
				if ($value !== null) {
					return $value;
				}
			}

		}

		public static function all() {
			Self::value('field', $_POST);
			return Self::$values['field'];
		}

		public static function old(string $name) {
			
			if (isset($_SESSION['blade']['input'][$name])) {
				return $_SESSION['blade']['input'][$name];
			}
		
		}


		// Add multiple file support later..
		// Keep it simple for now.
		public static function file($name) {

			if (isset($_FILES[$name]) && !empty($_FILES[$name])) {

				$count = (count($_FILES[$name]['name'])-1);
				for ($i=0; $i <= $count; $i++) {

					// Get real file extension.
					$extension = $_FILES[$name]['name'][$i];
					$extension = explode('.', $extension);
					$extension = end($extension);

					// Collect data, and push it to the Self::$values['files'].
					$data = array(
						'name' 		=> $_FILES[$name]['name'][$i],
						'type' 		=> $_FILES[$name]['type'][$i],
						'size' 		=> $_FILES[$name]['size'][$i],
						'error' 	=> $_FILES[$name]['error'][$i],
						'tmp_name' 	=> $_FILES[$name]['tmp_name'][$i],
						'extension' => $extension
					);

					Self::$values['files'][] = $data;

				}

			}

			return new static;

		}

			/* The functions beneath is only used with Self::file() */
			public function move($folder, $name = null) {

				$files = Self::$values['files'];

				$json = array();

				foreach ($files as $key => $file) {

					$rand = rand(1111, 9999) . '-' . rand(1111, 9999) . '.' . $file['extension'];

					if (is_array($name)) {
						$filename = (array_key_exists($key, $name))?$name[$key] . '.' . $file['extension']:$rand;
					} else {
						$filename = $rand;
					}


					if (move_uploaded_file($file['tmp_name'], $folder . $filename)) {
						$json[] = $filename;
					}

				}

				return json_encode($json);

			}

		/**************************************************************
			This is designed to SET/GET values from the values array.
			It is also used in Database
			-----------------------------------------------------------
			USE        : value('field', 'value')
			PARAM TYPES: value(string, string)
		**************************************************************/
		public static function value($input, $new = null) {

			// Check if the user wants to set the value.
			if ($new !== null){

				// Set the value
				Self::$values[$input] = $new;

			} else {

				// Get the value, and return it to use.
				return Self::$values[$input];

			}

		}

		public static function has(string $name) {
			
			return isset($_SESSION['blade']['errors'][$name]) ? true : false;
		
		}

		public static function message(string $name) {
			
			return $_SESSION['blade']['errors'][$name]['messages'][0]['message'];
		
		}

	}

?>
