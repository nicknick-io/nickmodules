<?php

	class Localization {

		public static $lang;

		public function __construct() {
			$this->lang = $GLOBALS['env']['Localization']['default'];
		}

		public static function locale($language) {

			$_SESSION['app']['lang'] = $language;
			Self::$lang = $language;

		}

		public static function translate($string) {

			Self::check();

			$notFound = $string;
			$string = explode('.', $string);
			$error = false;

			if (is_array($string)) {

				$file = "App/Localization/" . Self::$lang . "/" . $string[0] . ".php";
				$word = $string[1];

				if (file_exists($file)) {

					$string = require($file);
					if (array_key_exists($word, $string)) {
						echo $string[$word];
					} else {
						$error = true;
					}

				} else {
					$error = true;
				}

			} else {
				$error = true;
			}

			if ($error==true) {
				echo $notFound;
			}

		}

		public static function check() {

			$settings = require("app/config/app.php");

			if (isset($_SESSION['app']['lang'])) {
				Self::$lang = $_SESSION['app']['lang'];
			} else {
				Self::$lang = $settings['Localization']['default'];
			}

		}

	}

?>
