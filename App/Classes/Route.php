<?php
	class Route {

		static $as      = [],
			   $prefix  = [],
			   $baseurl = null,
			   $data = [
				   'routes' => null
			   ];

		public function __construct($baseurl) {

			Self::$baseurl = $baseurl;

		}

		public static function group($options, $closure) {

			// Check is we have some rules to follow.
			if (!is_array($options))
				die("Give the group some rules to follow!");

			// Check if we have a closure.
			if (!is_callable($closure))
				die("Give me a freaking closure.");

			// Fix extra building sheit, so it will take the last part off.
			Self::$as[] = $options['as'];
			if (isset($options['prefix'])) {
				Self::$prefix[] = $options['prefix'];
			}

			// Route class
			$route = new Route(Self::$baseurl);

			// Call closure
			$closure($route);

			// Unset the name after we have used it,
			// so we don't end up  naming them wrongly.
			foreach(Self::$as as $key => $as) {
				if ($as === $options['as']) {
					unset(Self::$as[$key]);
				}
			}

			if (isset($options['prefix'])) {
				foreach(Self::$prefix as $key => $prefix) {
					if ($prefix === $options['prefix']) {
						unset(Self::$prefix[$key]);
					}
				}
			}

		}

		public static function getURI() {
			return $_SERVER['REQUEST_URI'];
		}

		// Creates a GET method.
		public static function get($uri, $options) {

			Self::build($uri, 'GET', $options);

		}

		// Creates a POST method.
		public static function post($uri, $options) {

			Self::build($uri, 'POST', $options);

		}

		public static function build($uri, $method, $options) {

			$data = [
				'uri' => $uri,
				'method' => $method
			];

			# Just fixed the error where the closure has to be inside an array.
			if (!is_array($options)) {
				$data['closure'] = $options;
			}

			foreach($options as $key => $option) {

				if (is_callable($option)) {

					// ...
					$data['closure'] = $option;

				} elseif ($key == "as")  {

					$name = "";
					foreach(Self::$as as $as) {
						$name = $name . $as . '.';
					}

					// Set the name of the route.
					$data[$key] = $name . $option;

				} elseif ($key == "uses") {
					$data['closure'] = $option;
				} else {
					
					// Insert plain key and value.
					$data[$key] = $option;

				}

				if (!empty(Self::$prefix)) {
					$name = "";
					foreach(Self::$prefix as $prefix) {

						$count = strpos($name, $prefix);
						$name = (!$count)?str_replace($prefix, '', $name):'';
						# if ($count == false) { $name = str_replace($prefix, '', $name); }
						$name = $name . $prefix . '/';

					}

					$name = rtrim($name, '/');

					// Set the name of the route.
					$data['prefix'] = $name;
					if ($data['prefix'] != "") {
						$data['uri'] = $data['prefix'] . '/' . $uri;

						// This fixed the grouping bug.
						$data['uri'] = rtrim($data['uri'], '//');
					}

				}

			}

			Self::$data['routes'][] = $data;

		}

		public static function uriHandler() {

			// Get the current project folder.
			$folder = Self::$baseurl . '/';

			// Get the URI and remove the
			// project folder form it.
			$uri = Self::getURI();
			$uri = str_replace($folder, '', $uri);

			// Check if the string had a slash in the beginning.
			if (substr($uri, 0, 1) == '/' && strlen($uri) > 2) {
				$uri = substr($uri, 1, strlen($uri)-1);
			}

			// Return the URI.
			return $uri;

		}

		public function parameters($url) {

			if (!empty($url)) {

				// Get all the parameters.
				$handler = preg_match_all('~{(.*?)}~', $url, $parameters);

				// return parameters.
				return $parameters[1];

			}

		}

		public function brainTwist() {

			$routes = Route::$data['routes'];
			$method = $_SERVER['REQUEST_METHOD'];

			$currentRoute = false;

			foreach($routes as $key => $route) {

				$string = $route['uri'];
				$current = Route::uriHandler();
				$parameters = Route::uriArray($route['uri']);

				if (count($parameters) > 0) {
					foreach($parameters as $key => $parameter) {
						$string = str_replace('{'. $key .'}', $parameter, $string);
					}
				}

				// Check if we have a match.
				if ($string === $current && ($method == $route['method'])) {

					// Set the current route and call the closure.
					$currentRoute = $key;
					// $checkClosure = (!$route['closure'])?false:true;
					$closure = $route['closure'];

					// Check to see if the $closure is callable.
					if (is_callable($closure)) {

						// Check if there is some parameters.
						if (count($parameters) > 0) {
							return call_user_func_array($closure, $parameters);
						}

						// Check if there is zero parameters.
						if (count($parameters) == 0) {
							return call_user_func($closure);
						}

					}

					// Check to see if the $closure isn't callable.
					if (!is_callable($closure)) {

						// Explode 'uses' closure by @.
						$n = explode('@', $closure);
						$closure = [$n[0], $n[1]];

						// Check if there is some parameters.
						if (count($parameters) > 0) {
							return call_user_func_array($closure, $parameters);
						}

						// Check if there is zero parameters.
						if (count($parameters) == 0) {
							return call_user_func($closure);
						}

					}

				}

			}

			if ($currentRoute == false) {
				die("Route not defined.");
			}

		}

		public static function url($name, $parameters = false) {

			// Generate baseurl.
			$url = (!empty($_SERVER['HTTPS']))?'https://':'http://';
			$url = $url . $_SERVER['HTTP_HOST'];
			// $url = $url . Self::$baseurl . '/';

			// Selected route
			$selected = false;

			// Get routes.
			$routes = Self::$data['routes'];
			foreach($routes as $key => $route) {

				if ($route['as'] === $name) {
					$selected = $route['uri'];
				}

			}

			if ($selected !== false) {

				// The assembled url.
				if (!$parameters) {

					// Check if we have a home route.
					if ($selected !== "/") {
						return $url.'/'.$selected;
					} else {
						return $url;
					}

				} else {

					$url = $url . '/' . $selected;
					foreach($parameters as $key => $parameter) {
						$url = str_replace('{' . $key . '}', $parameter, $url);
					}

					return $url;

				}

			}

		}

		// Needs work.
		public function uriArray($uri) {

			$data = [];

			// Take the URI, with data.
			$uriData = Self::uriHandler();
			$uriData = explode('/', $uriData);

			// Take the URI from routes.
			$uriRoute = $uri;
			$uriRoute = explode('/', $uriRoute);

			for ($i=0; $i<=count($uriData)-1; $i++) {

				if (array_key_exists($i, $uriRoute)) {

					if (preg_match('~{(.*?)}~', $uriRoute[$i], $uri)) {

						// URI replacement.
						$uri = $uri[0];
						$uri = str_replace('{', '', $uri);
						$uri = str_replace('}', '', $uri);

						// Push to array.
						$data[$uri] = $uriData[$i];

					}

				} 

			}

			return $data;

		}

		public static function showRoutes() {
			echo "
			<style type='text/css'>
				table {
						width:100%;
						border-collapse: collapse;
				}
				table, td, th {
					border:1px solid #303030;
				}
				td {
					padding:4px 6px;
				}
				th {
					text-align:left;
				}
			</style>";
			$routes = Self::$data['routes'];
			echo "<table>";
				echo "<tr>";
					echo "<th>URI</th>";
					echo "<th>Method</th>";
					echo "<th>Route name</th>";
					echo "<th>Action</th>";
				echo "</tr>";

			foreach($routes as $route):

				$prefix  = (!empty($route['prefix']))?$route['prefix']:'';
				$closure = (is_callable($route['closure']))?"Closure":$route['closure'];

				echo "<tr>";
					echo "<td>" . $route['uri'] . "</td>";
					echo "<td>" . $route['method'] . "</td>";
					echo "<td>" . $route['as'] . "</td>";
					echo "<td>" . $closure . "</td>";
				echo "</tr>";

			endforeach;
			echo "</table>";

		}

	}
