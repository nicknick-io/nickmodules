<?php

	class File {

		public static function get($string) {

			if (!empty($string)) {

				$content = @file_get_contents($string);
				if ($content) {
					return $content;
				} else {
					return "<b>File not found: </b>" . $string;
				}

			}
		
		}

	}