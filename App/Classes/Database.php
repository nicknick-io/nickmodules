<?php

	class Database {

		// This Should be public
		// When developing.
		public static $values = [
			'get' => null,
			'like' => [],
			'orLike' => [],
			'table' => null,
			'limit' => null,
			'where' => null,
			'insert' => null,
			'order' => [],
			'group' => [],
			'joins' => [],
			'bindings' => []
		];

		function __construct() {

			// Get the settings from the array.
			$this->env = $GLOBALS['env'];

			$host = $this->env['Database']['host'];
			$user = $this->env['Database']['user'];
			$pass = $this->env['Database']['pass'];
			$name = $this->env['Database']['name'];

			// Forsøg at oprette forbindelse til database
			try {

				// Save database connection to $this->pdo
				$this->pdo = new \PDO('mysql:host='.$host.';dbname='.$name, $user, $pass);

				// Set attributes to $this->pdo
				$this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
				$this->pdo->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);


				$this->pdo->exec("set names utf8");

			} catch(\PDOException $e) {

			    echo $e->getMessage();
			    exit();

			}

		}

		public function __destruct() {
			unset($this->pdo);
		}


		/**
		*	Used only for developing purposes.
		*	@param array $fields
		*	@return $string
		*************************************/
		public function raw($fields = null) {

			if (!empty($this->value('table'))) {

				// Put array or null
				$this->value('get', $fields);

				// Define selectors used
				$fields = $this->selector('fields');
				$table  = $this->selector('table');
				$where  = $this->selector('where');
				$limit  = $this->selector('limit');
				$order  = $this->selector('orderBy');
				$like   = $this->selector('like');
				$orlike = $this->selector('orLike');
				$join   = $this->selector('join');
				$group  = $this->selector('groupBy');

				$string = "SELECT {$fields} FROM {$table} {$join} {$where} {$like} {$orlike} {$group} {$order} {$limit}";

				// Reset data..
				$this->reset();

				return $string;

			}

		}

		/**
		*	Used to select a table.
		*	@param string $table
		*	@return static
		*************************************/
		public static function table($table = null) {

			// Reset data..
			Self::reset();

			if (!empty($table)) {
				Self::value('table', $table);
			}

			// return $this;
        	return new static;
		}

		/**
		*	This method is used define where statements.
		*	@param array $where
		*	@return static
		*************************************/
		public function where($where = array()) {

			if (!empty($where)) {
				$this->value('where', $where);
			}

			return $this;

		}

		/**
		*	Used to define data to insert.
		*	@param array $insert
		*	@return boolean
		*************************************/
		public function insert($insert = array()) {

			if ($this->value('table') !== null) {
				$this->value('insert', $insert);

				if ($this->value('insert') !== null) {

					$insert = $this->selector('insert');
					$table = $this->selector('table');

					// Build query
					$query = $this->pdo->prepare("INSERT INTO {$table} {$insert}");

					// Bind values to the query
					foreach (Self::$values['bindings'] as $binding) {
						$query->bindValue($binding['bind'], $binding['value'], \PDO::PARAM_STR);
					}

					// Execute the query and check if the query was successful.
					if ($query->execute()) {
						Self::reset();
						return true;
					} else {
						return false;	
					}

				}

			}

		}

		/**
		*	Used to define update data.
		* 	This method takes an array: 'column' => 'New Value'
		*	@param array $update
		*	@return boolean
		*************************************/
		public function update($update = array()) {

			# Makes sure the values is converted properly.
			$this->value('get', $update);

			# Get all the data needed for this job to get done.
			$update = $this->selector('update');
			$table 	= $this->selector('table');
			$where 	= $this->selector('where');
			$like 	= $this->selector('like');
			$orlike = $this->selector('orLike');

			# This prepares the query so we can do what we're supposed to.
			$query = $this->pdo->prepare("UPDATE {$table} SET {$update} {$where} {$like} {$orlike}");

			# For security reasons we need to bind the values to the query. 
			foreach (Self::$values['bindings'] as $binding) {
				$query->bindValue($binding['bind'], $binding['value'], \PDO::PARAM_STR);
			}

			# Execute the query. Check if it were a success.
			if ($query->execute()) {

				Self::reset();
				return true;

			} else {

				return false;

			}

		}

		/**
		*	Used to define what to order by.
		*	This method takes an array.
		*
		*	@param array $orderBy
		*	@return 
		*************************************/
		public function orderBy($orderBy = array()) {

			foreach($orderBy as $key => $value) {
				Self::$values['order'][$key] = $value;
			}

			return $this;

		}

		/**
		*	Used for grouping things.
		*	This method takes an array.
		*	@param array $groupBy
		*	@return 
		*************************************/
		public function groupBy($groupBy = array()) {

			foreach($groupBy as $value) {
				array_push(Self::$values['group'], $value);
			}

			return $this;

		}

		/**
		*	Used to delete from the database.
		* 	This method takes no parameters.
		*
		*	@return boolean
		*************************************/
		public function delete() {

			$table = $this->selector('table');
			$where = $this->selector('where');
			$like  = $this->selector('like');
			$orlike = $this->selector('orLike');

			$query = $this->pdo->prepare("DELETE FROM {$table} {$where} {$like} {$orlike}");

			// Bind values to the query
			foreach (Self::$values['bindings'] as $binding) {
				$query->bindValue($binding['bind'], $binding['value'], \PDO::PARAM_STR);
			}

			// Execute the query
			if ($query->execute()) {

				// Reset data..
				$this->reset();
				return true;

			} else {

				return false;

			}

		}

		/**
		*	Used to get the count of the selected items.
		*
		*	@return integer
		*************************************/
		public function count() {

			# Get all the data we need to use.
			$fields = $this->selector('fields');
			$table  = $this->selector('table');
			$where  = $this->selector('where');
			$like   = $this->selector('like');
			$orlike = $this->selector('orLike');

			# Prepare the query.
			$query = $this->pdo->prepare("SELECT {$fields} FROM {$table} {$where} {$like} {$orlike}");

			# Bind all the values to the query.
			foreach (Self::$values['bindings'] as $binding) {
				$query->bindValue($binding['bind'], $binding['value'], \PDO::PARAM_STR);
			}

			Self::value('bindings', []);

			# Execute the query.
			if ($query->execute()) {
				
				# Return the row count.
				return $query->rowCount();
				
			} 

			# If everything is failing, return 0.
			return 0;

		}

		/**
		*	This method retreives all the results.
		*	@param array 
		*	@return object
		*************************************/
		public function get($get = null) {

			if ($this->value('table')) {

				# Make sure the data is converted properly.
				$this->value('get', $get);

				# Get all the data we need to use.
				$fields = $this->selector('fields');
				$table 	= $this->selector('table');
				$where 	= $this->selector('where');
				$limit 	= $this->selector('limit');
				$order  = $this->selector('orderBy');
				$like  	= $this->selector('like');
				$orlike = $this->selector('orLike');
				$join   = $this->selector('join');
				$group  = $this->selector('groupBy');

				# Build the query
				$query = $this->pdo->prepare("SELECT {$fields} FROM {$table} {$join} {$where} {$like} {$orlike} {$group} {$order} {$limit}");

				# Bind the values to the query.
				foreach (Self::$values['bindings'] as $binding) {
					$query->bindValue($binding['bind'], $binding['value'], \PDO::PARAM_STR);
				}

				# Execute the query.
				$query->execute();

				# Return all the results.
				return $query->fetchAll(\PDO::FETCH_OBJ);
				
			}

		}

		public function first($value = null) {

			if ($this->value('table')) {

				// Set the data to the array.
				$this->value('get', $value);

				// Defined selectors
				$fields = $this->selector('fields');
				$table 	= $this->selector('table');
				$where 	= $this->selector('where');
				$like  	= $this->selector('like');
				$orlike = $this->selector('orLike');
				$join   = $this->selector('join');
				$order  = $this->selector('orderBy');
				$group  = $this->selector('groupBy');

				// Build the query
				$query = $this->pdo->prepare("SELECT {$fields} FROM {$table} {$join} {$where} {$like} {$orlike} {$group} {$order} LIMIT 1");

				// Bind values to the query
				foreach (Self::$values['bindings'] as $binding) {
					$query->bindValue($binding['bind'], $binding['value'], \PDO::PARAM_STR);
				}

				// Execute the query
				$query->execute();

				// Reset data..
				$this->reset();

				return $query->fetch(\PDO::FETCH_OBJ);
				

			}

		}

		/**************************************************************
			This is the function that people DON'T like.. The limiter.
			It can set the limit of returned rows.
			-------------------------------------------------------
			USE         : limit(10)
			PARAM TYPES : limit(integer)
		**************************************************************/
		public function limit($limit) {

			if (!empty($limit) && is_numeric($limit)) {
				$this->value('limit', $limit);
			} // Or proceed without any errors.

			return $this;

		}

		/**************************************************************
			This is the function that makes the LIKE tick.
			It's used to search in fields.
		**************************************************************/
		public function like($array) {

			if (!empty($array) && is_array($array)) {

				foreach($array as $key => $value) {
					Self::$values['like'][$key] = $value;
				}

			} // Or proceed without any errors.

			return $this;

		}
		/**************************************************************
			This is the function that makes the LIKE tick.
			It's used to search in fields.
		**************************************************************/
		public function orLike($array) {

			if (!empty($array) && is_array($array)) {

				foreach($array as $key => $value) {
					Self::$values['orLike'][$key] = $value;
				}

			} // Or proceed without any errors.

			return $this;

		}

		public function join($table, $from, $selector, $to) {

			if (!empty($table) && !empty($from) && !empty($selector) && !empty($to)) {

				$data = "INNER JOIN {$table} ON {$from} {$selector} {$to} ";
				array_push(Self::$values['joins'], $data);

			}

			return $this;

		}

		public function leftJoin($table, $from, $selector, $to) {

			if (!empty($table) && !empty($from) && !empty($selector) && !empty($to)) {

				$data = "LEFT JOIN {$table} ON {$from} {$selector} {$to} ";
				array_push(Self::$values['joins'], $data);

			}

			return $this;

		}

		public function rightJoin($table, $from, $selector, $to) {

			if (!empty($table) && !empty($from) && !empty($selector) && !empty($to)) {

				$data = "RIGHT JOIN {$table} ON {$from} {$selector} {$to} ";
				array_push(Self::$values['joins'], $data);

			}

			return $this;

		}


		public function atos($array, $type) {

			if (!empty($array) && !empty($type)) {
				// Define variables.
				$string = "";

				foreach($array as $key => $value) {

					$random = ':nick' . rand(1, 9999) . rand(111, 9999) . rand(200, 2000);
					$bind = [
						'bind' => $random,
						'value' => $value
					];

					array_push(Self::$values['bindings'], $bind);

					switch($type) {

						case 'key':
							// Sort out all keys, and merge them into a string.
							$string = $string . $key . ', ';
						break;

						case 'value':
							// Sort out all values, and merge them into a string.
							$string = $string . $random . ', ';
						break;

						case 'insert-value':
							// Making a special rule for the insert values... It's not good.
							$string = $string . '\'' . $random . '\', ';
						break;

						case 'anding':
							$string = $string . $key . ' = ' . $random . ' AND ';
						break;

						case 'update':
							// Sort out all keys/values, and merge them into a string.
							$string = $string . $key . ' = ' . $random . ', ';
						break;

						case 'like':
							// Lets create a string for the "like" thingy..
							// However this goes
							$string = $string . ' `' . $key . '` LIKE ' . $random . ' AND ';
						break;

						case 'orLike':
							$string = $string . ' `' . $key . '` LIKE ' . $random . ' OR ';
						break;

						case 'insert':
							$string = $string . $key . '-' . $random . '|';
						break;
					}
				}

				// Remove the last defined "character"
				// This wont mess up because it's genius.
				$string = rtrim($string, ', ');
				$string = rtrim($string, 'AND ');
				$string = rtrim($string, '|');
				$string = rtrim($string, 'OR ');

				// return result
				return $string;
			}
		}

		/**************************************************************
			This is designed to SET/GET values from the values array.
			-----------------------------------------------------------
			USE        : limit(10)
			PARAM TYPES: limit(integer)
		**************************************************************/
		public static function value($input, $new = null) {

			// Check if the user wants to set the value.
			if ($new !== null){

				// Set the value
				Self::$values[$input] = $new;

			} else {

				// Get the value, and return it to use.
				return Self::$values[$input];

			}

		}

		// Used to define a selector..
		// Or i call it the string maker...
		// Because there is way too many combinations...
		public function selector($what) {
			switch($what){

				// Used to define what should match..
				case 'where':
					$where = ($this->value('where') !== null)?$this->atos($this->value('where'), 'anding'):"";
					return (!empty($where))?'WHERE ' . $where:"";
				break;

				// Used to define what fields that should be collected from the database.
				case 'fields':

					if (Self::$values['get'] !== null) {
						$string = "";
						foreach(Self::$values['get'] as $field) {
							$string = $string . $field . ', ';
						}
						$string = rtrim($string, ', ');
					}

					return (Self::$values['get'] == null)?"*":$string;

				break;

				// Used to define which keys and values that should be updated.
				case 'update':
					return ($this->value('get') !== null)?$this->atos($this->value('get'), 'update'):"";
				break;

				// Used to define a limit of records returned.
				case 'limit':
					$limit = ($this->value('limit') !== null)?$this->value('limit'):"";
					return (!empty($limit))?'LIMIT ' . $this->value('limit'):"";
				break;

				// Used to make specific searches in selected fields.
				case 'like':
					$like = ($this->value('like') !== null)?$this->value('like'):"";
					$where = ($this->value('where') == null)?"WHERE":"AND";
					return (!empty($like))?$where . $this->atos($like, 'like'):"";
				break;

				// Used to make specific searches in selected fields.
				case 'orLike':
					$orlike = ($this->value('orLike') !== null)?$this->value('orLike'):"";
					$where = ($this->value('like') == null && $this->value('where'))?"":"OR";
					return (!empty($orlike))?$where . $this->atos($orlike, 'orLike'):"";
				break;

				// Used to define what table we're playing with.
				case 'table':
					return $this->value('table');
				break;

				case 'join':
					$string = "";
					foreach(Self::$values['joins'] as $join) {
						$string = $string . $join . ' ';
					}
					return $string;
				break;

				case 'orderBy':

					if (!empty(Self::$values['order'])) {
						$string = "ORDER BY ";
						foreach(Self::$values['order'] as $key => $value) {
							$string = $string . $key . ' ' . $value . ', ';
						}

						$string = rtrim($string, ', ');

						return $string;
					}

				break;

				case 'groupBy':

					if (!empty(Self::$values['group'])) {
						$string = "GROUP BY ";

						foreach(Self::$values['group'] as $value) {
							$string = $string . $value . ', ';
						}

						$string = rtrim($string, ', ');

						return $string;
					}

				break;

				case 'insert':

					// Never mind this code... ITS SHIT!
					// I need to figure out a new way to do this.
					// But it works for now..
					$keys = "";
					$values = "";

					$data = $this->value('insert');
					$info = $this->atos($data, 'insert');

					$items = explode('|', $info);
					foreach ($items as $item) {
						$item = explode('-', $item);
						$keys = $keys . $item[0] . ', ';
						$values = $values . $item[1] . ', ';
					}
					$keys = rtrim($keys, ', ');
					$values = rtrim($values, ', ');
					return '(' . $keys . ') VALUES (' . $values . ')';

				break;

			}
		}

		// This is used to reset the properties.
		public static function reset() {

			Self::value('get', []);
			Self::value('like', []);
			Self::value('table', '');
			Self::value('limit', '');
			Self::value('where', '');
			Self::value('insert', '');
			Self::value('order', []);
			Self::value('group', []);
			Self::value('joins', []);
			Self::value('bindings', []);

		}

	}

?>
