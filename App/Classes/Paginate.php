<?php
	
	class Paginate {

		static  $data = null,			# Keeps all the data.
				$dataCount = null,		# Count all the the data
				$perPage = null,		# Records per page.
				$pageData = null,		# Page data.
				$pageDataCount = null,	# Counts all the current page data.
				$pageNumbers = null,	# Keeps all the page numbers.
				$currentPage = null;	# Returns integer of current page.


		/**
		*	This method is used to define a new paginator.
		*
		*	@return Nothing
		**********************************************************/
		public static function make($data, $perPage = 10) {

			# Save the data.
			Self::$data = $data;

			# Save per page integer.
			Self::$perPage = (0 > $perPage)?10:$perPage;		
			
			# Count all records parsed into the paginator.
			Self::$dataCount = count(Self::$data);

			# Initialize page numbers.
			Self::pageNumbers();

			# Initialize page data.
			Self::pageData();

			# Check if the current page is available.
			Self::pageAvailable();

		}

		/**
		*	This method is used to calculate the page numbers.
		*
		* 	@return Nothing
		**********************************************************/
		public static function pageNumbers() {

			# Calculate the number of pages we have.
			$pageNumberCount = ceil(Self::$dataCount / Self::$perPage);

			# Check if we have any pages.
			# We always need at least one page.
			$pageNumberCount = ($pageNumberCount > 0)?$pageNumberCount:1;

			# Create a datacontainer for the page numbers.
			$pageNumbers = [];

			# Make a loop that can parse the page numbers.
			for ($i=0; $i < $pageNumberCount; $i++) { 
				
				# Push every page number into the datacontainer.
				$pageNumbers[] = ($i + 1);

			}

			# Set the current page.
			Self::$currentPage = (!isset($_GET['page']) && !empty($_GET['page']))?1:$_GET['page'];

			# This is when we have no data to process.
			if (empty($paginate))
				$paginate[] = 1;

			# Save the page numbers to the public variable.
			Self::$pageNumbers = $pageNumbers;

		}

		/**
		*	This is used to calculate which items that 
		*	should be shown on each page.
		*
		*	@return Nothing
		**********************************************************/
		public static function pageData() {

			# Make an array for the current page data.
			$pageData = [];

			# Load the data we need.
			$data = Self::$data;
			$perPage = Self::$perPage;
			$currentPage = Self::$currentPage;

			# Process all page data for the current page.
			foreach($data as $key => $value) {

				# Calculate the offset and limit.
				$offset = ($currentPage * $perPage) - $perPage;
				$limit	= ($currentPage * $perPage) - 1;

				# Find the page data we need.
				if (($key > $offset || $key == $offset) && ($limit > $key || $limit == $key)) {

					$pageData[] = (object)$value;

				}

			}

			# Save the current page data.
			Self::$pageData = $pageData;

			# Save the count of current page data.
			Self::$pageDataCount = count($pageData);

		} 

		/**
		*	This method is used for calculating data information.
		*	
		*	@return string $text
		**********************************************************/
		public static function dataWidget() {

			# Get the data we need to use.
			$perPage = Self::$perPage;
			$currentPage = Self::$currentPage;
			$pageDataCount = Self::$pageDataCount;
			$dataCount = Self::$dataCount;

			# Calculate from and to.
			$from = ($perPage * $currentPage) - $perPage + 1;
			$to = $from + $pageDataCount - 1;

			# Make the text that is shown to the user.
			$text = "
				Showing <strong>{$from}</strong> - 
				<strong>{$to}</strong> of 
				<strong>{$dataCount}</strong> items
			";

			# Return the text.
			return $text;

		}

		/**
		*	Used to get information about the page information.
		*
		*	@return string $text
		**********************************************************/
		public static function pageWidget() {

			# Define the data we need to use.
			$currentPage = Self::$currentPage;
			$pageNumberCount = count(Self::$pageNumbers);

			# Make the text that is shown to the user.
			$text = "Page <strong>{$currentPage}</strong> of <strong>{$pageNumberCount}</strong>";

			# Return the text.
			return $text;

		}

		/**
		*	Used to check if a user tried to access a page 
		*	that doesn't exist.
		*
		*	@return Nothing 
		**********************************************************/
		public static function pageAvailable() {

			# Define what we need to use.
			$pageNumbers = Self::$pageNumbers;
			$currentPage = Self::$currentPage;

			# Check if the page number is greater that
			# the ones that exists.
			if (end($pageNumbers) < $currentPage) {

				# Send the person back on track.
				return redirect('?page=' . end($pageNumbers));

			}

			# Check if thepage number is lower than
			# the ones that exists.
			if ($pageNumbers[0] > $currentPage) {
				
				# Send the user back on track.
				return redirect('?page=' . $pageNumbers[0]);
			
			}

		}

		/**
		*	Used to check if the current page is the first.
		*	(Meant for the Blade Handler)
		*
		*	@return boolean $isFirst; 
		**********************************************************/
		public static function isFirst() {

			# Define the data we need to use.
			$pageNumbers = Self::$pageNumbers;
			$currentPage = Self::$currentPage;

			# Check if the current page is the first.
			$isFirst = ($pageNumbers[0] == $currentPage)?true:false;

			# Return the status.
			return $isFirst;

		}

		/**
		*	Used to check if the current page is the last.
		*	(Meant for the Blade Handler)
		*
		*	@return Nothing 
		**********************************************************/
		public static function isLast() {

			# Define the data we need to use.
			$pageNumbers = Self::$pageNumbers;
			$currentPage = Self::$currentPage;

			# Check if the current page is tha last page.
			$isLast = (end($pageNumbers) == $currentPage)?true:false;

			# Return the status.
			return $isLast;

		}

		/**
		*	Used to calculate the previous page number.
		*	(Meant for the Blade Handler)
		*
		*	@return boolean $previous
		**********************************************************/
		public static function previous() {

			# Define the data we need to use.
			$currentPage = Self::$currentPage;

			# calculate the previous page.
			$previous = ($currentPage != 1)?$currentPage-1:1;

			# Return the status.
			return $previous;

		}

		/**
		*	Used to calculate the next page number.
		*	(Meant for the Blade Handler)
		*
		*	@return boolean $next
		**********************************************************/
		public static function next() {

			# Define the data we need to use.
			$currentPage = Self::$currentPage;
			$pageNumbers = Self::$pageNumbers;

			# Calculate the next page.
			$next = end($pageNumbers);
			$next = ($next != $currentPage)?$currentPage+1:$next;

			# Return the page number.
			return $next;

		}

		/**
		*	Used to check if the page is active.
		*	(Meant for the Blade Handler)
		*
		*	@return Nothing 
		**********************************************************/
		public static function isActive($key) {

			# Define the data we need to use.
			$currentPage = Self::$currentPage;

			# Take the key that comes through and plus one.
			$key = $key + 1;

			# Calculate the state.
			$isActive = ($currentPage == $key)?true:false;

			# Return the status.
			return $isActive;

		}

	}