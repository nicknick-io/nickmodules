<?php

	class Request {

		public static function get($name, $function) {

			$data = (object)Self::params();
			if (isset($_GET[$name])) {
				return $function($data);
			}

		}

		public static function post($name, $function) {

			$data = (object)Self::params();
			if (isset($_POST[$name])) {
				return $function($data);
			}

		}

		public static function server($string = null) {

			if ($string !== null){
				return $_SERVER[$string];
			} else {
				return $_SERVER;
			}

		}

		public static function params() {

			return [
				'GET' => $_GET, 
				'POST' => $_POST
			];

		}

		public static function redirect($url) {

			if (!empty($url)) {
				header('Location: ' . $url);
			}

			return new static;
		
		}

		public static function withInput(array $fields) {
			
			// Save the input in the session, so we 
			// can access the input fields in the view.
			$_SESSION['blade']['input'] = $fields;
			
			return new static;

		}

		public static function withErrors(array $errors) {
			
			$_SESSION['blade']['errors'] = $errors;

			return new static;
		
		}

	}
?>