<?php

	/*******************************************************************
	*																   *
	*	This file is basically a big container filled with settings.   *
	*	NOTE: !This may never be defined as a public variable!         *
	*																   *
	*******************************************************************/
	return [

		// Database settings.
		'Database' => [
			'host' => '127.0.0.1',
			'name' => 'database',
			'user' => 'root',
			'pass' => ''
		],

		// Validation messages.
		'Validation' => [
			'min' 		=> "{field} skal mindst være {value} tegn langt.",
			'max' 		=> "{field} skal være under {value} tegn langt.",
			'same' 		=> "{field} skal matche med {value}.",
			'email' 	=> "{field} skal være en gyldig email.",
			'digits' 	=> "{field} skal være {value} tegn langt.",
			'unique'	=> "{field} skal være unik i tabellen {value}.",
			'numeric' 	=> "{field} må kun indeholde tal.",
			'required' 	=> "{field} skal være udfyldt."

		],

		// Email settings.
		'Email' => [
			'host'  	=> 'smtp.domain.com',
			'port' 		=> '26',
			'username' 	=> 'smtp_username',
			'password'  => 'smtp_password',
			'security'	=> 'tls'
		],

		// Localization
		'Localization' => [
			'default' 	=> 'en'
		],

		// Input settings { Coming Soon }
		'Input' => [
			'except' => ['password']
		],
		
	];
