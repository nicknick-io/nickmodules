<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>NickModules</title>
        <style media="screen">
            @import url(https://fonts.googleapis.com/css?family=Lato:400,300,100);

            body {
                margin: 0;
            }

            div {
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translateY(-50%) translateX(-50%);
            }

            h1 {
                font-size: 6.8em;
                font-family:Lato;
                margin: 0;
                font-weight: 100;
                opacity: 0.9;
            }

            span {
                color:#114875;
            }
        </style>
    </head>
    <body>
        <div>
            <h1>
                <span>Nick</span>Modules
            </h1>
        </div>
    </body>
</html>
