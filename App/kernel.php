<?php
	ob_start();

	// start session
	session_start();

	// Load the settings.
	$env = require_once("Config/app.php");

	// Load 3rd party vendors.
	// Good enough for now! I got bigger fish to fry..

		// PHP Mailer
		require_once("Vendor/PHPMailer/class.phpmailer.php");
		require_once("Vendor/PHPMailer/class.smtp.php");
		require_once("Vendor/PHPMailer/class.pop3.php");

		// PHP WideImage
		require_once("Vendor/WideImage/WideImage.php");


	// Loading all the classes from the following folders.
	function __autoload($class) {

		# App/classes
		$classes = __DIR__ . "/Classes/{$class}.php";
		if (file_exists($classes)) {
			require_once($classes);
		}

		# App/classes
		$controllers = __DIR__ . "/Controllers/{$class}.php";
		if (file_exists($controllers)) {
			require_once($controllers);
		}

	}

		# Translate function
		function lang($item) {
			return Localization::translate($item);
		}

		# View function
		function View($file, $item = null) {

			# Call the make
			$view = View::make($file);

			# Check if we have defined some variables.
			# That we need to send through to the view.
			if ($item) $view->with($item);

			# Return the view
			return $view;

		}

		// Route function
		function route($route, $parameters = null) {

			$route = Route::url($route, $parameters);
			return $route;

		}

		// Redirect function
		function redirect($link) {
			return Request::redirect($link);
		}

		// Asset function
		function asset($file) {

			// Generate baseurl.
			$url = (!empty($_SERVER['HTTPS']))?'https://':'http://';
			$url = $url . $_SERVER['HTTP_HOST'] . $url . '/Assets/' . $file;

			return $url;

		}
?>
